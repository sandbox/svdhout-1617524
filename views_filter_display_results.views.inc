<?php
/**
 * @file
 * Provides the views data and handlers for Views Filter Display Results
 * Author : Steven Van den Hout (steven.vandenhout@calibrate.be)
 */

/**
 * Implements hook_views_plugins().
 */
function views_filter_display_results_views_plugins() {
  return array(
    'argument default' => array(
      'views_filter_display_results' => array(
        'title' => t('Content ID from Display'),
        'handler' => 'views_filter_display_results_plugin_argument_default_display_results',
        'path' => drupal_get_path('module', 'views_filter_display_results') . '/includes',
      ),
    ),
  );
}
