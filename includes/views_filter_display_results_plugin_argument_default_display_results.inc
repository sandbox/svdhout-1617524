<?php

/**
 * @file
 *  Allows you to add arguments based on a display in that view.
 */

/**
 * Argument default with the display results
 */
class views_filter_display_results_plugin_argument_default_display_results extends views_plugin_argument_default {
  function option_definition() {
    $options = parent::option_definition();
    $options['display_id'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $displays = array();
    foreach ($this->view->display as $display_id => $display) {
      $displays[$display_id] = $display->display_title;
    }
    $form['display_id'] = array(
      '#type' => 'select',
      '#title' => t('Display'),
      '#default_value' => $this->options['display_id'],
      '#options' => $displays,
      '#description' => t('The machine name for the attachment to get the results from'),
    );
    
  }
  
  function get_argument() {
    $result = array();
    $attachment = views_get_view($this->view->name);
    $attachment->set_display($this->options['display_id']);
    $attachment->preview();
    foreach ($attachment->result as $row) {
      $result[] = $row->nid;
    }
    if (count($result) > 0) return implode(',', $result);
  }
}

